import { APIGatewayProxyResult, SQSEvent } from 'aws-lambda'
import Knex from 'knex'

const host = process.env.DATABASE_HOST
const port = process.env.DATABASE_PORT
const user = process.env.DATABASE_USER
const password = process.env.DATABASE_PASSWORD
const database = process.env.DATABASE_SCHEMA

const connection = {
  ssl: { rejectUnauthorized: false },
  host,
  port: Number(port),
  user,
  password,
  database,
}

// Create the connection
const knex = Knex({
  client: 'pg',
  connection,
})

export const handler = async (
  event: SQSEvent
): Promise<APIGatewayProxyResult> => {
  console.log({ Records: event.Records })

  await knex('log')
    .select('*')
    .then((data) => {
      console.log({ dataLogsFromDB: data })
    })
    .catch((err) => {
      console.log({ errFromKnex: err })
    })

  return {
    statusCode: 200,
    body: JSON.stringify('Hello from Lambda desde SQS!'),
  }
}

// exports.handler = async (event) => {
//   console.log({ event: event.Records })

//   const response = {
//       statusCode: 200,
//       body: JSON.stringify('Hello from Lambda!'),
//   };
//   return response;
// };
