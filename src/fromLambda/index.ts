import { APIGatewayProxyResult, APIGatewayEvent } from 'aws-lambda'
import { SQSClient, SendMessageCommand } from '@aws-sdk/client-sqs'

interface IHubspotEventChange {
  eventId: number
  subscriptionId: number
  portalId: number
  appId: number
  ocurredAt: number
  subscriptionType: string
  attemptNumber: number
  objectId: number
  propertyName: string
  propertyValue: string
  changeSource: string
}

export const handler = async (
  event: APIGatewayEvent
): Promise<APIGatewayProxyResult> => {
  const requestFromHubspot = JSON.parse(
    event.body as string
  ) as IHubspotEventChange[]

  console.log({ requestFromHubspot })

  const client = new SQSClient({ region: 'us-east-1' })
  const command = new SendMessageCommand({
    MessageBody: JSON.stringify(requestFromHubspot),
    QueueUrl:
      'https://sqs.us-east-1.amazonaws.com/587401939662/LambdaPrueba.fifo',
    MessageGroupId: requestFromHubspot[0].objectId.toString(),
  })

  return await client
    .send(command)
    .then((result: any) => {
      console.log({ result })
      return {
        statusCode: 200,
        body: JSON.stringify({
          body: result,
        }),
      }
    })
    .catch((error: any) => {
      console.log({ error })
      return {
        statusCode: 200,
        body: JSON.stringify({
          body: error,
        }),
      }
    })
}
