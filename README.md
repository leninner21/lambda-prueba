##  Lifting of lambda functions.

This repository was created with the purpose of helping the avoid data loss in the flow of leads.

## Prerequisites
- Create a user in the aws **IAM** service.
- Create a user group and add the last created user to that group.
- Adding `AWSLambda_FullAccess` permissions to group.
- Added the **ACCESS_KEY**, **SECRET_ACCESS_KEY**, **REGION_NAME** to `.env ` line.
- Type the name of the function to be deploy as **FUNCTION_NAME**.


## Instalacion
- **Packages of proyect.**
`npm install || npm i`

- **AWS Cli.**
Execute the following command line on your terminal (Linux):
`curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" unzip awscliv2.zip sudo ./aws/install`

> (Reference of installation)
> https://docs.aws.amazon.com/es_es/cli/latest/userguide/getting-started-install.html

- Execute the following command line and type credentials **ACCESS_KEY**, **SECRET_ACCESS_KEY** and **REGION_NAME**.
`aws configure`

## Deploy

- Execute the following command line for deploy a function define on "package.json"
`npm run run-deploy-lambda`
